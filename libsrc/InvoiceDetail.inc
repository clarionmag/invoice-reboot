    

InvoiceDetail       Class,Type,module('InvoiceDetail.clw'),link('InvoiceDetail.clw',1)
GetDiscount             procedure,real
GetExtended             procedure,real
GetSavings              procedure,real
GetTax                  procedure,real
GetTotal                procedure,real
Init                    procedure(real price,long quantity)
SetDiscountRate         procedure(real discountRate)
SetTaxRate              procedure(real taxRate)
price                   decimal(10,2),PRIVATE
discountRate            decimal(5,2),private
quantity                long,PRIVATE
taxRate                 decimal(5,2),private
                    End 

