                                                   member
                                                   map
                                                   end



   include('InvoiceDetail.inc'),once

InvoiceDetail.GetDiscount                          PROCEDURE
Extended                                              decimal(10,2)
Result                                                decimal(10,2)
   code 
   Extended = self.GetExtended()
   Result = self.discountRate / 100 * Extended
   return Result
    
    
InvoiceDetail.GetExtended                          PROCEDURE
   code
   return self.price  * self.quantity
    
InvoiceDetail.GetSavings                           PROCEDURE
   code 
   return self.GetDiscount()

InvoiceDetail.GetTax                               procedure     
result                                                decimal(10,2)
Extended                                              decimal(10,2)
Discount                                              decimal(10,2)
   code 
   Extended = self.GetExtended()
   !Discount = self.GetDiscount()
   result = self.taxRate / 100 * (Extended - Discount)
   return result
   
InvoiceDetail.GetTotal                             PROCEDURE
   code 
   return self.GetExtended() - self.GetDiscount() + self.GetTax()    
    
InvoiceDetail.Init                                 procedure(real price,long quantity)    
   code 
   self.price = price
   self.quantity = quantity
    
InvoiceDetail.SetDiscountRate                      procedure(real discountRate)
   code 
   self.discountRate = discountRate

InvoiceDetail.SetTaxRate                           procedure(real taxRate)
   code 
   self.taxRate = taxRate

