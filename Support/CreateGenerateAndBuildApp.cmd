@echo off

set AppName=%1
set WorkingDir=%2
set ClaInterface="C:\Program Files (x86)\UpperParkSolutions\claInterface\ClaInterface.exe"
set ClarionCL=e:\Clarion\Clarion10\bin\ClarionCL.exe

if "%3"=="" (
	set vcDirectory=vc%AppName%
) else (
	set vcDirectory=%3
)

echo AppName %AppName%
echo WorkingDir %WorkingDir%
echo ClaInterface %ClaInterface%
echo vcDirectory %vcDirectory%

echo %ClaInterface% COMMAND=BuildCompileApp Input=%WorkingDir%\%vcDirectory%\%AppName%
%ClaInterface% COMMAND=BuildCompileApp Input=%WorkingDir%\%vcDirectory%\%AppName%

rem echo %ClaInterface% COMMAND=BuildApp Input=%WorkingDir%\%vcDirectory%\%AppName%
rem %ClaInterface% COMMAND=BuildApp Input=%WorkingDir%\%vcDirectory%\%AppName%
rem echo %ClarionCL% /up_createapp %WorkingDir%\%AppName%.ups %WorkingDir%\%AppName%.app 
rem %ClarionCL% /up_createapp %WorkingDir%\%AppName%.ups %WorkingDir%\%AppName%.app 
rem echo %ClaInterface% COMMAND=CompileApp Input=%WorkingDir%\%vcDirectory%\%AppName%
rem %ClaInterface% COMMAND=CompileApp Input=%WorkingDir%\%vcDirectory%\%AppName%

exit /b 0
