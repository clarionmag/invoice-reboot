# README #

This is an open source remake of the pretty ancient Clarion Invoice example app. The initial commit is the same code as I used for the ClarionMag article series 
[The Problem with Embeds, Revisited](https://clarionmag.jira.com/wiki/spaces/clarion/pages/399575/The+Problem+with+Embeds+Revisited)

## Why does this repo even exist? ##

It sure isn't here an example of fine coding standards. Frankly the app is ugly and borderline unusable, at least so far. 
The purpose of this app is to allow any number of Clarion developers to experience the process of development using a shared Git repository and the Upper Park toolset, and to 
learn how to move code out of embed points and into reusable, testable classes. For more on that subject please read the article series above. 

## How do I get set up? ##

The apps in this repository are only available as APV files. You will need the 
[Upper Park Solutions Version Control Interface](https://clarionshop.com/ViewProductForCustomer?Change_btn=Change&SPro__RecordID=1419) in order to create
the apps from their APV files.

Invoice.app is easy enough to use. Just load up Invoice.sln in Clarion, right-click on the app in the solution explorer, and import using the Upper Park tooling. 
You should be able to compile and run. 

InvoiceDetail_Tests.app is a bit trickier. You need to have ClarionTest up and running and the templates available. I'll post more on the test setup at a later date.

## Planned improvements ##

- build server 
- execute automated tests as part of the build process
